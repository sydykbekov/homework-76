import {START_REQUEST, SUCCESS_REQUEST, ERROR_REQUEST, LAST_DATE} from "./action";
const initialState = {
    messages: [],
    lastDate: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case ERROR_REQUEST:
            alert(action.error);
            return {...state, loading: false};
        case SUCCESS_REQUEST:
            return {...state, messages: action.messages, lastDate: action.messages[action.messages.length - 1].dateTime, loading: false};
        case LAST_DATE:
            return {...state, lastDate: action.date};
        default:
            return state;
    }
};

export default reducer;