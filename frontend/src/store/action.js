import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const LAST_DATE = 'LAST_DATE';

const startRequest = () => {
    return {type: START_REQUEST};
};

const successRequest = (messages) => {
    return {type: SUCCESS_REQUEST, messages};
};

const errorRequest = (error) => {
    return {type: ERROR_REQUEST, error};
};

const lastDate = (date) => {
    return {type: LAST_DATE, date};
};

const scrollToBottom = () => {
    const posts = document.getElementById('posts');
    posts.scrollTop = posts.scrollHeight - posts.clientHeight;
};

const setInt = (dispatch, getState) => {
    this.interval = setInterval(() => {
        axios.get(`messages?datetime=${getState().lastDate}`).then(response => {
            if (response.data.length !== 0) {
                const currentMessages = getState().messages;
                let newMessages = currentMessages.concat(response.data);
                dispatch(successRequest(newMessages));
                dispatch(lastDate(newMessages[newMessages.length - 1].dateTime));
                scrollToBottom();
            }
        });
    }, 2000);
};

export const clearInt = () => {
    clearInterval(this.interval);
};

export const getMessages = () => {
    return (dispatch, getState) => {
        dispatch(startRequest());
        axios.get('messages').then(response => {
            dispatch(successRequest(response.data));
            scrollToBottom();
            setInt(dispatch, getState);
        }, error => {
            dispatch(errorRequest(error));
        });
    };
};

export const sendMessage = state => {
    clearInt();
    return (dispatch, getState) => {
        dispatch(startRequest());
        axios.post('messages', state).then(() => {
            setInt(dispatch, getState);
        }, error => {
            dispatch(errorRequest(error));
        });
    };
};
