import React, {Component} from 'react';
import Message from '../../components/Message/Message';
import './Messenger.css';
import {connect} from "react-redux";
import {sendMessage, getMessages, clearInt} from "../../store/action";
import Preloader from '../../assets/Preloader.gif';

class Messenger extends Component {
    state = {
        author: '',
        message: ''
    };

    changeValues = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    componentWillUnmount() {
        this.props.clearInt();
    }

    componentDidMount() {
        this.props.getMessages();
    }

    render() {
        return (
            <div className="chat">
                <img style={{display: this.props.loading ? 'block' : 'none'}} src={Preloader} alt="loading"/>
                <div className="chat-header">
                    <h4>Messenger</h4>
                </div>
                <div className="chat-body" id="posts">
                    {this.props.messages.map(message => {
                        return <Message
                            key={message.id}
                            author={message.author}
                            message={message.message}
                            time={message.dateTime}
                        />
                    })}
                </div>
                <div className="chat-footer">
                    <input onChange={this.changeValues} value={this.state.author} type="text"
                           name="author"
                           placeholder="Author"/>
                    <textarea placeholder="Message..." onChange={this.changeValues} name="message"
                              value={this.state.message}/>
                    <button onClick={() => this.props.sendMessage(this.state)}>Send</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        sendMessage: (state) => dispatch(sendMessage(state)),
        getMessages: () => dispatch(getMessages()),
        clearInt: () => dispatch(clearInt())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Messenger) ;