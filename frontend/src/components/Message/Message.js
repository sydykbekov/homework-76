import React from 'react';
import './Message.css';

const Message = props => {
    return(
        <div className="message">
            <span>{props.author}</span>
            <p>{props.message}</p>
            <span>{props.time}</span>
        </div>
    )
};

export default Message;