const express = require('express');
const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        if (req.query.datetime) {
            const date = new Date(req.query.datetime);
            if (isNaN(date.getDate())) {
                res.status(400).send({"error": "Invalid datetime"});
            } else {
                res.send(db.getMessagesByDate(req.query.datetime));
            }
        } else {
            res.send(db.getMessages());
        }
    });

    router.post('/', (req, res) => {
        if (req.body.author === '' || req.body.message === '') {
            res.status(400).send({error: "Author and message must be present in the request"});
        } else {
            db.addMessage(req.body).then(result => {
                res.send(result);
            });
        }
    });

    return router;
};

module.exports = createRouter;