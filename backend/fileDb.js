const fs = require('fs');
const nanoid = require('nanoid');

let data = null;

module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./messages.json', (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data = JSON.parse(result);
                    resolve();
                }
            });
        })
    },
    getMessages: () => {
        return data.slice(-30);

    },
    getMessagesByDate: datetime => {
        const index = data.findIndex(message => datetime === message.dateTime);
        return data.slice(index + 1, data.length);
    },
    addMessage: message => {
        const mess = message;
        mess.dateTime = new Date().toISOString();
        mess.id = nanoid();
        data.push(mess);
        return new Promise((resolve, reject) => {
            fs.writeFile('./messages.json', JSON.stringify(data, null, 2), err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(mess);
                }
            });
        });
    }
};