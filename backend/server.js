const express = require('express');
const app = express();
const fileDb = require('./fileDb');
const cors = require('cors');
const messages = require('./app/messages');

const port = 8000;

app.use(express.json());
app.use(cors());

fileDb.init().then(() => {
    app.use('/messages', messages(fileDb));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});